### About this repository
HKO-test is a side project to test public Open Data APIs from Hong Kong Observatory.

#### `hko-cli`
a light-weight API as cli wrapper written in bash, usage example:
```
$ chmod +x ./hko-cli
```

#### `hko-api-test-runner`
a simple commands-as-testcase script with built-in test examples, which leverages `hko-cli` for testing. Run `./hko-api-test-runner` to see the usage.

A testcase example template:
(Content in between `<`,`>` must be replaced, including the brackets.)
```Bash
Case-<Title of test case>() {
    eval $Begin

    echo "Test description, or anything relevant."

    <Commands or script to run the test.>

    eval $End
}
```
Test summary is saving to `./TestResultSummary.txt` and reset by each test run.

##### Requirement and Installation
- bash (v3.2.57+)
- jq
- curl
- yajsv `$ go install github.com/neilpa/yajsv@v1.4.0`
  - go1.17

### References
- Official API document: [HKO_Open_Data_API_Documentation.pdf](https://data.weather.gov.hk/weatherAPI/doc/HKO_Open_Data_API_Documentation.pdf)
- JSON Schema validator implementations: [link](https://json-schema.org/implementations.html)
